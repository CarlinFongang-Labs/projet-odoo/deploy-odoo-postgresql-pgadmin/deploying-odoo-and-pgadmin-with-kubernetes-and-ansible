# Mise en œuvre d'un CI/CD via GitLab-CI, Ansible et Kubernetes

_______


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Introduction
L'objectif principal de ce lab est de mettre en place un pipeline CI/CD utilisant GitLab-CI, Ansible et Kubernetes qui permettra d'assurer le déploiement et gérer des applications de manière automatisée sur une infrastructure cloud.

## Pourquoi le choix de ces outils ? 

### GitLab-CI 

**Intégration native avec GitLab :** GitLab-CI est intégré de manière native avec GitLab, offrant une solution tout-en-un pour le contrôle de version et l'intégration continue. Cette intégration réduit la complexité de gestion des différents outils et centralise les workflows de CI/CD.

**Configuration par code :** GitLab-CI utilise des fichiers .gitlab-ci.yml pour la configuration des pipelines, permettant une gestion par code de l'ensemble du processus de CI/CD, ce qui améliore la reproductibilité et la maintenance.

Facilité d'utilisation et communauté active : GitLab-CI est soutenu par une large communauté d'utilisateurs et de développeurs qui contribuent régulièrement à son amélioration et à l'extension de ses fonctionnalités.

### Ansible
**Simplicité et efficacité :** Ansible utilise une architecture sans agent, où les commandes sont exécutées via SSH. Cela élimine le besoin de serveurs ou de daemons supplémentaires, simplifiant ainsi la structure de gestion et réduisant les points de défaillance.

**Idempotence :** Ansible garantit que même si un playbook est exécuté plusieurs fois sur le même environnement, le résultat final reste le même. Cela apporte une fiabilité cruciale pour les déploiements automatisés.

### Kubernetes (kubeadm)

**Standard de l'industrie pour l'orchestration des conteneurs :** Kubernetes est devenu le standard pour l'orchestration des conteneurs, offrant une plateforme robuste pour automatiser le déploiement, la mise à l'échelle et la gestion des applications conteneurisées.

**Flexibilité et scalabilité :** Kubernetes permet une gestion fine des ressources et une scalabilité automatique en fonction de la charge, essentielle pour les applications critiques.


# 1. **Prérequis**
   ## 1.1. Mise en place de Kubernetes avec kubeadm sur les serveurs cibles.

   [Découvrez comment mettre en place un cluster kubeadm en suivant ce lab](https://gitlab.com/CarlinFongang-Labs/kubernetes/lab1.1-install-kubeadm.git)

   ## 1.2. Installation et configuration d'Ansible pour l'automatisation des déploiements.

   [Découvrez comment installer Ansible en suivant ce lab](https://gitlab.com/CarlinFongang-Labs/Ansible/lab1-install-ansible.git)

# 2. **Configuration de GitLab-CI**

  ## 2.1. Mise en place de runner privé GitLab-CI.

  [Découvrez comment mettre en place placeun runner GitLab-CI en suivant ce lab](https://gitlab.com/CarlinFongang-Labs/gitlab-cicd/lab1-build#mise-en-place-de-runner-priv%C3%A9s)

  ## 2.2. Configuration des variables d'environnement globales pour les déploiements.



  ## 2.3. Intégration avec les répertoires de code source des applications (Odoo, pgAdmin, ic-webapp).

# 3. **Développement des scripts Ansible**
   ## 3.1. Création de rôles Ansible pour le déploiement des applications.
   ## 3.2. Automatisation des tâches de configuration des environnements et de déploiement des applications.

# 4. **Création des manifests Kubernetes pour définir les déploiements, services, volumes persistants, et configurations réseau nécessaires pour chaque application.**


   ## 4.1. Création d'un name space
````bash
nano webapp-namespace.yml
````

````yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ic-webapp-space
````

````bash
 kubectl apply -f webapp-namespace.yml
````
![alt text](image.png)


 ## 4.2. PostgreSQL

### 4.2.1. Création du PV pour la base de données PostgreSQL

````yaml
nano postgres-pv.yml
````

````yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: postgres-pv
  labels:
    type: local
    env: prod
  namespace: ic-webapp-space
spec:
  capacity:
    storage: 10Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: manual
  hostPath:
    path: "/postgres-data/postgres-pv"
````

````bash
kubectl apply -f postgres-pv.yml
````

![alt text](image-5.png)

### 4.2.2. Création du PVC pour la base de données PostgreSQL

````bash
nano postgres-pvc.yml
````

````yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: postgres-pvc
  labels:
    app: odoo
    env: prod
  namespace: ic-webapp-space
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 3Gi
  storageClassName: manual
````

````bash
kubectl apply -f postgres-pvc.yml
````

![alt text](image-6.png)


   ### 4.2.3. PostgreSQL | ClusterIP

````nano
nano postgres-clusterip.yml
````

````yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    app: postgres
    env: prod
  name: postgres
  namespace: ic-webapp-space
spec:
  ports:
  - port: 5432
    protocol: TCP
    targetPort: 5432
  selector:
    app: postgres
  type: ClusterIP
````

````bash
kubectl apply -f postgres-clusterip.yml
````

![alt text](image-4.png)

   ### 4.2.4. PostgreSQL | Deployment

````
nano postgres-deployment.yml
````

````yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postgres
  labels:
    app: postgres
    env: prod
  namespace: ic-webapp-space
spec:
  replicas: 1
  selector:
    matchLabels: 
      app: postgres
  template:
    metadata:
      labels:
        app: postgres
    spec:
      containers:
      - image: postgres:latest
        name: postgres
        env:
        - name: POSTGRES_DB
          value: postgres
        - name: POSTGRES_USER
          value: admin
        - name: POSTGRES_PASSWORD
          valueFrom:
            secretKeyRef:
              name: postgres
              key: POSTGRES_PASSWORD
        ports:
        - containerPort: 5432
          name: postgres
      volumes:
      - name: postgres-data
        persistentVolumeClaim:
          claimName: postgres-pvc
````

Conversion des secret en base 64
````bash
echo -n 'odoo' | base64
````

Vérifier les secrets

````bash
kubectl get secret postgres-secret -o yaml
````

Créer les repertoire utile au deploiement de PostgreSQL

````bash
sudo mkdir -p /var/lib/postgresql/data /data/postgres
````

Mise à jour du secret pour le deploiement de postgres

````bash
kubectl create secret generic postgres -n ic-webapp-space --from-literal=POSTGRES_PASSWORD=odoo
````

Exécution du deployement

````bash
kubectl apply -f postgres-deployment.yml
````

![alt text](image-7.png)


## 4.3. Odoo

   ### 4.3.1. Odoo | Persistent Volume 

````bash
nano odoo-pv.yml
````

````yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: odoo-pv
  labels:
    type: local
    env: prod
  namespace: ic-webapp-space
spec:
  capacity:
    storage: 10Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: standard
  hostPath:
    path: /odoo-data/odoo-pv

````nano
kubectl apply -f odoo-pv.yml
````

![alt text](image-1.png)

   ### 4.3.2. Odoo | Persistent Volume Claim 

````bash
nano odoo-pvc.yml
````

````yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: odoo-pvc
  labels:
    app: odoo
    env: prod
  namespace: ic-webapp-space
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 5Gi
````

````bash
kubectl apply -f odoo-pvc.yml
````
![alt text](image-2.png)


   ### 4.3.3. Odoo | Manifest de déploiement
   Création du ConfigMap

````bash
nano odoo.conf
````

````bash
[options]
admin_passwd = admin
db_host = postgres
db_port = 5432
db_user = admin
db_password = False
addons_path = /mnt/extra-addons,/usr/lib/python3/dist-packages/odoo/addons
````

    Création d'un ConfigMap sous kubernetes

````bash
kubectl create configmap odoo-config --from-file=odoo.conf -n ic-webapp-space
````

![alt text](image-9.png)

   <!-- Création du passeword odoo

````yaml
kubectl create secret generic odoo --from-literal=PASSWORD='odoo' -n ic-webapp-space
```` -->

````bash
nano odoo-deployment.yml
````

````yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: odoo
  labels:
    app: odoo
    env: prod
  namespace: ic-webapp-space
spec:
  replicas: 1
  selector:
    matchLabels:
      app: odoo
  template:
    metadata:
      labels:
        app: odoo
    spec:
      containers:
      - name: odoo
        image: odoo
        env:
        - name: USER
          value: admin
        - name: HOST
          value: postgres
        - name: PASSWORD
          valueFrom:
            secretKeyRef:
              name: odoo
              key: PASSWORD 
        ports:
        - containerPort: 8069
          name: odoo
        volumeMounts:
        - name: odoo-data
          mountPath: /etc/odoo
        - name: odoo-storage
          mountPath: /var/lib/odoo
        - name: extra-addons-data
          mountPath: /mnt/extra-addons
      volumes:
      - name: odoo-data
        hostPath:
          path: /data_docker/config
      - name: extra-addons-data
        hostPath:
          path: /data_docker/addons
      - name: odoo-storage
        persistentVolumeClaim:
          claimName: odoo-pvc
````

````bash
sudo mkdir -p /etc/odoo /var/lib/odoo /odoo-data/config /odoo-data/addons
sudo chmod 777 -R /etc/odoo /var/lib/odoo /odoo-data/config /odoo-data/addons
````

Lancement de la stack Odoo

````bash
kubectl apply -f odoo-configmap.yml
kubectl apply -f odoo-deployment.yml
````


![alt text](image-8.png)




   ### 4.1.4. Odoo nodeport

````bash
nano odoo-nodeport.yml
````

````bash
apiVersion: v1
kind: Service
metadata:
  name: odoo
  labels:
    app: odoo
    env: prod
  namespace: ic-webapp-space
spec:
  type: NodePort
  selector:
    app: odoo
  ports:
    - protocol: TCP
      port: 8069
      targetPort: 8069
      nodePort: 30069
````

````bash
kubectl get services -n ic-webapp-space
````

![alt text](image-3.png)


Check de la flotte de pods

````bash
kubectl get svc,po -n ic-webapp-space
````

   ### 4.1.5. Gestion des secrets


   ## 4.2. Sécurisation des secrets Kubernetes et gestion des configurations sensibles.

# 5. **Pipeline CI/CD dans GitLab-CI**
   ## 5.1. **Build** : Construction des images Docker pour de l'application vitrice ic-webapp.
   ## 5.2. **Test** : Exécution de tests automatisés pour vérifier la stabilité et la performance des builds.
   ## 5.3. **Release** : Marquage des builds réussis avec des tags de version.
   ## 5.4. **Deploy** : Déploiement des applications dans l'environnement de test suivi par le déploiement en production.

# 6. **Automatisation du déploiement**
   ## 6.1. Utilisation d'Ansible pour exécuter les manifests Kubernetes.
   ## 6.2. Configuration des pipelines pour déclencher des déploiements via des merges dans des branches spécifiques ou des tags.

# 7. **Surveillance et Maintenance**
   ## 7.1. Mise en place de la surveillance des applications pour garantir la haute disponibilité.
   ## 7.2. Configuration des alertes pour réagir rapidement en cas de défaillance.

# 8. **Documentation et Formation**
   ## 8.1. Rédaction d'une documentation détaillée sur la configuration du système et les procédures d'exploitation.
   ## 8.2. Formation des équipes de développement et d'exploitation sur le nouveau système de CI/CD.

# 9. **Test de fonctionnement**
   ## 9.1. Tests de validation pour s'assurer que le pipeline fonctionne comme prévu et que les applications répondent aux exigences.

